package web

import (
	"embed"
	"fmt"
	"net/http"
)

//go:embed dist/*
var WebDataFS embed.FS

func DistSubdirFileServer(root http.FileSystem) http.Handler {
	return http.FileServer(&subdirectoryFS{root})
}

type subdirectoryFS struct {
	root http.FileSystem
}

func (s *subdirectoryFS) Open(name string) (http.File, error) {
	if name == "/" {
		name = ""
	}

	return s.root.Open(fmt.Sprintf("dist%s", name))
	//return s.root.Open(name)
}
