package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func checkForUpdate(checkUpdateUrl string) {
	httpClient := http.Client{Timeout: 2*time.Second}
	res, err := httpClient.Get(checkUpdateUrl)
	if err != nil {
		return
	}

	defer res.Body.Close()
	latestVersionBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	latestVersion := strings.TrimSpace(string(latestVersionBytes))

	if latestVersion != Version {
		fmt.Printf("A new version is available. Current version is %s, latest is %s\n", Version, latestVersion)
	}
}
