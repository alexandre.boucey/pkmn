package main

import (
	"fmt"
	"go.uber.org/zap/zapcore"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"pkmn/internal/gdb"
	"pkmn/internal/live"
	"pkmn/internal/server"
	"time"

	"gopkg.in/yaml.v2"

	"go.uber.org/zap"
)

const UndefinedVersionString = "undefined"

var Version string = UndefinedVersionString

func IsReleaseVersion() bool {
	return Version != UndefinedVersionString
}

type config struct {
	ListenAddr     string        `yaml:"listen-addr"`
	EmulatorAddr   string        `yaml:"emulator-addr"`
	PollInterval   time.Duration `yaml:"poll-interval"`
	Debug          bool          `yaml:"debug"`
	SpriteUrl      string        `yaml:"sprite-url"`
	CheckUpdate    bool          `yaml:"check-update"`
	CheckUpdateUrl string        `yaml:"check-update-url""`
}

func newLogger(debug bool) (*zap.Logger, error) {
	useDebug := !IsReleaseVersion() || debug
	var lvl zapcore.Level = zap.InfoLevel
	if useDebug {
		lvl = zap.DebugLevel
	}
	cfg := zap.Config{
		Level:            zap.NewAtomicLevelAt(lvl),
		Development:      useDebug,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	return cfg.Build()
}

func main() {
	fmt.Printf("~ Alkov Pkmn Overlay v.%s ~\n", Version)
	var cfg = config{
		"127.0.0.1:49498",
		"127.0.0.1:2345",
		5,
		false,
		"https://www.serebii.net/art/th/%d.png",
		true,
		"https://alkov.gitlab.io/pkmn/version.txt",
	}

	loadConfig(&cfg)

	if IsReleaseVersion() && cfg.CheckUpdate {
		checkForUpdate(cfg.CheckUpdateUrl)
	}

	var logger *zap.Logger
	logger, _ = newLogger(cfg.Debug)
	defer logger.Sync()

	gdbClient := gdb.NewClient(cfg.EmulatorAddr, logger)
	interactor := live.NewInteractor(gdbClient, logger)

	s := server.NewServer(cfg.ListenAddr, interactor, cfg.PollInterval*time.Second, cfg.SpriteUrl, logger)

	logger.Info("Starting server", zap.String("addr", cfg.ListenAddr))
	s.Listen()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	if awaitGdb(gdbClient, stop, logger) {
		_ = gdbClient.Continue()

		err := interactor.LoadOffsetsFromRom()
		if err != nil {
			logger.Fatal("Unable to find offsets for this rom",
				zap.Error(err),
			)
		}

		logger.Info("Connected to emulator, starting poll")

		select {
		case <-s.Loop():
		case <-stop:
		}

	}

	s.Stop()
	_ = gdbClient.Close()
}

func loadConfig(c *config) {
	b, err := ioutil.ReadFile("config.yaml")
	err = yaml.Unmarshal(b, c)
	if err != nil {
		log.Println("Cannot load config:", err)
	}

	if c.PollInterval < 5 {
		c.PollInterval = 5
	}
}

func awaitGdb(gdbClient *gdb.Client, stop chan os.Signal, logger *zap.Logger) bool {
	for i := 0; i < 10; i++ {
		select {
		case <-stop:
			return false
		default:
			// continue
		}

		err := gdbClient.Connect()
		if err == nil {
			return true
		}

		logger.Info("Cannot connect to emulator", zap.String("addr", gdbClient.Addr()), zap.Int("try", i))
		time.Sleep(5 * time.Second)
	}

	log.Fatalln("Failed to connect to emulator, exiting.")
	return false
}
