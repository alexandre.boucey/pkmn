#Pkmn
Live memory manipulation for Gen3 pkmn

## Endpoints
- `/ws`: websocket endpoint, dispatch team change events
- `/trigger/<mod>/<target>`: trigger in-game actions. `target` is either `team` or `random`.

### mod: `status`
Change the status of a random pokemon or the whole team. By default a random status is selected, but it aloes accept a 
GET parameter `type` to one of the status below or `clear` to clear status.
```
sleep
poison
burn
freeze
paralysis
```

### mod: `hp`
Change the HP of a random pokemon or the whole team. The effect has to be specified using the `action` GET parameter
- `halve`: remove 50% of the remaining HP of the target (cannot kill)
- `one`: set the HP of the target to 1 (cannot resurrect)
- `heal`: heal a pokemon by 33% of hs max HP (can resurrect)

_Note: only some combinations are available._


## Sprite URL
- `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%d.png`
- `https://www.serebii.net/art/th/%d.png`