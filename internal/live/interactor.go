package live

import (
	"bytes"
	"encoding/binary"
	"errors"
	"go.uber.org/zap"
	"math"
	"math/rand"
	"pkmn/internal/gdb"
	"pkmn/internal/pkmn"
)

var ErrNoAvailablePokemon = errors.New("no available pokemon for action")
var ErrIncompatibleRom = errors.New("incompatible rom")

type privateRomHeader struct {
	GameTitle [12]byte
	GameCode  [4]byte
	MakerCode [2]byte
}

func (h privateRomHeader) GameTitleString() string {
	return string(h.GameTitle[:])
}

func (h privateRomHeader) GameCodeString() string {
	return string(h.GameCode[:])
}

func (h privateRomHeader) MakerCodeString() string {
	return string(h.MakerCode[:])
}

// Interactor struct
type Interactor struct {
	gdb     *gdb.Client
	logger  *zap.Logger
	offsets *pkmn.RomOffset
}

// NewInteractor fn
func NewInteractor(gdb *gdb.Client, logger *zap.Logger) *Interactor {
	return &Interactor{
		gdb,
		logger,
		nil,
	}
}

func (w *Interactor) LoadOffsetsFromRom() (err error) {
	m, err := w.gdb.ReadMemory(0x08000000+0x0A0, 18)
	if err != nil {
		return
	}

	var header privateRomHeader

	d := bytes.NewBuffer(m)
	err = binary.Read(d, binary.LittleEndian, &header)

	w.logger.Info("Got header from ROM",
		zap.String("GameTitle", header.GameTitleString()),
		zap.String("GameCode", header.GameCodeString()),
		zap.String("MakerCode", header.MakerCodeString()),
	)

	if header.MakerCodeString() != "01" {
		return ErrIncompatibleRom
	}

	offsets, ok := pkmn.RomOffsets[header.GameCodeString()]
	if !ok {
		return ErrIncompatibleRom
	}

	w.offsets = &offsets

	return
}

func (w *Interactor) IsInCombat() (b bool, err error) {
	m, err := w.gdb.ReadMemory(w.offsets.BattleTilemapA, 4)
	if err != nil {
		return
	}

	var currentBattleFlag uint32

	d := bytes.NewBuffer(m)
	err = binary.Read(d, binary.LittleEndian, &currentBattleFlag)

	return currentBattleFlag != 0, nil
}

// GetPlayerTeam fetch the current party pokemons
func (w *Interactor) GetPlayerTeam() (party [6]pkmn.Pkmn, err error) {
	whole := make([]byte, 600)
	decoded, err := w.gdb.ReadMemory(w.offsets.PlayerTeam, 300)
	if err != nil {
		return
	}
	copy(whole[:300], decoded)

	decoded, err = w.gdb.ReadMemory(w.offsets.PlayerTeam+300, 300)
	if err != nil {
		return
	}
	copy(whole[300:], decoded)

	d := bytes.NewBuffer(whole)
	err = binary.Read(d, binary.LittleEndian, &party)
	return
}

// GetOpponentTeam fetch the current opponent party pokemons
func (w *Interactor) GetOpponentTeam() (party [6]pkmn.Pkmn, err error) {
	whole := make([]byte, 600)
	decoded, err := w.gdb.ReadMemory(w.offsets.OpponentTeam, 300)
	if err != nil {
		return
	}
	copy(whole[:300], decoded)

	decoded, err = w.gdb.ReadMemory(w.offsets.OpponentTeam+300, 300)
	if err != nil {
		return
	}
	copy(whole[300:], decoded)

	d := bytes.NewBuffer(whole)
	err = binary.Read(d, binary.LittleEndian, &party)
	return
}

func (w *Interactor) GetBattle() (battle [4]pkmn.BattlePkmn, err error) {
	m, err := w.gdb.ReadMemory(w.offsets.BattleTeam, 352)
	if err != nil {
		return
	}

	d := bytes.NewBuffer(m)
	err = binary.Read(d, binary.LittleEndian, &battle)
	return
}

func (w *Interactor) SetTeamStatusCondition(s pkmn.StatusCondition) (err error) {
	defer w.gdb.Continue()
	err = w.gdb.Interrupt()
	if err != nil {
		return
	}

	statusBytes := s.Bytes()

	party, err := w.GetPlayerTeam()
	if err != nil {
		return
	}

	for i, p := range party {
		if p.Personality != 0 {
			offset := pkmn.OffsetTeamPkmn(w.offsets.PlayerTeam, uint32(i), pkmn.OffsetTeamPkmnFieldCondition)
			err = w.gdb.WriteMemory(offset, statusBytes)
			if err != nil {
				return
			}
		}
	}

	battle, err := w.GetBattle()
	if err != nil {
		return
	}
	for i, b := range battle {
		if b.Personality == party[0].Personality || b.Personality == party[1].Personality {
			offset := w.offsets.OffsetBattlePkmn(uint32(i), pkmn.OffsetBattlePkmnFieldCondition)
			err = w.gdb.WriteMemory(offset, statusBytes)
			if err != nil {
				return
			}
		}
	}

	return
}

func (w *Interactor) SetRandomStatusCondition(s pkmn.StatusCondition) (err error) {
	defer w.gdb.Continue()
	err = w.gdb.Interrupt()
	if err != nil {
		return
	}

	statusBytes := s.Bytes()
	var availablePkmn []int
	party, err := w.GetPlayerTeam()
	if err != nil {
		return
	}
	for j, p := range party {
		if p.Personality == 0 {
			continue
		}

		// If we are setting a clear status, don't pick already cleared status
		if s.IsClear() && p.StatusCondition.IsClear() {
			continue
		}

		// if we are setting a effect status, don't pick already affected pkmn
		if !s.IsClear() && !p.StatusCondition.IsClear() {
			continue
		}

		if p.CurrentHealth == 0 {
			continue
		}

		availablePkmn = append(availablePkmn, j)
	}

	if len(availablePkmn) == 0 {
		return ErrNoAvailablePokemon
	}

	i := availablePkmn[rand.Intn(len(availablePkmn))]
	offset := pkmn.OffsetTeamPkmn(w.offsets.PlayerTeam, uint32(i), pkmn.OffsetTeamPkmnFieldCondition)
	err = w.gdb.WriteMemory(offset, statusBytes)
	if err != nil {
		return
	}

	battle, err := w.GetBattle()
	if err != nil {
		return
	}
	for i, battlePkmn := range battle {
		if battlePkmn.Personality != party[i].Personality {
			continue
		}
		offset = w.offsets.OffsetBattlePkmn(uint32(i), pkmn.OffsetBattlePkmnFieldCondition)
		err = w.gdb.WriteMemory(offset, statusBytes)
		if err != nil {
			return
		}
	}

	return nil
}

func clampHP(p *pkmn.Pkmn, target uint16) uint16 {
	if target < 0 {
		target = 0
	}

	if target > p.TotalHealth {
		target = p.TotalHealth
	}

	return target
}

func (w *Interactor) HalveTeamHP() (err error) {
	defer w.gdb.Continue()
	err = w.gdb.Interrupt()
	if err != nil {
		return
	}

	party, err := w.GetPlayerTeam()
	if err != nil {
		return
	}

	for i, p := range party {
		if p.Personality != 0 {
			targetHealth := clampHP(&p, uint16(math.Ceil(float64(p.CurrentHealth)/2.0)))
			buff := new(bytes.Buffer)
			err = binary.Write(buff, binary.LittleEndian, targetHealth)
			if err != nil {
				return
			}
			offset := pkmn.OffsetTeamPkmn(w.offsets.PlayerTeam, uint32(i), pkmn.OffsetTeamPkmnFieldCurrentHealth)
			err = w.gdb.WriteMemory(offset, buff.Bytes())
			if err != nil {
				return
			}
		}
	}

	return
}

func (w *Interactor) HealTeamHP() (err error) {
	defer w.gdb.Continue()
	err = w.gdb.Interrupt()
	if err != nil {
		return
	}

	party, err := w.GetPlayerTeam()
	if err != nil {
		return
	}

	for i, p := range party {
		if p.Personality != 0 {
			targetHealth := clampHP(&p, p.CurrentHealth+uint16(math.Ceil(float64(p.TotalHealth)*0.33)))
			buff := new(bytes.Buffer)
			err = binary.Write(buff, binary.LittleEndian, targetHealth)
			if err != nil {
				return
			}
			offset := pkmn.OffsetTeamPkmn(w.offsets.PlayerTeam, uint32(i), pkmn.OffsetTeamPkmnFieldCurrentHealth)
			err = w.gdb.WriteMemory(offset, buff.Bytes())
			if err != nil {
				return
			}
		}
	}

	return
}

func (w *Interactor) SetRandomToOneHP() (err error) {
	defer w.gdb.Continue()
	err = w.gdb.Interrupt()
	if err != nil {
		return
	}

	var availablePkmn []int
	party, err := w.GetPlayerTeam()
	if err != nil {
		return
	}
	for j, p := range party {
		if p.Personality == 0 {
			continue
		}

		if p.CurrentHealth == 0 {
			continue
		}

		availablePkmn = append(availablePkmn, j)
	}

	if len(availablePkmn) == 0 {
		return ErrNoAvailablePokemon
	}

	i := availablePkmn[rand.Intn(len(availablePkmn))]
	offset := pkmn.OffsetTeamPkmn(w.offsets.PlayerTeam, uint32(i), pkmn.OffsetTeamPkmnFieldCurrentHealth)
	err = w.gdb.WriteMemory(offset, []byte{1, 0})
	if err != nil {
		return
	}

	return
}
