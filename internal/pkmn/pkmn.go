package pkmn

import (
	"bytes"
	"encoding/binary"
	"errors"
)

// Marking typesafety
type Marking byte

// Sorting markings
const (
	Circle   Marking = 0x00
	Square   Marking = 0x01
	Triangle Marking = 0x02
	Heart    Marking = 0x03
)

// Language typesafety
type Language uint16

//
const (
	Japanese Language = 0x0201
	English  Language = 0x0202
	French   Language = 0x0203
	Italian  Language = 0x0204
	German   Language = 0x0205
	Korean   Language = 0x0206
	Spanish  Language = 0x0207
	Egg      Language = 0x0601
)

// Personality type
type Personality uint32

// GrowthData type
type GrowthData struct {
	Specie     uint16
	Item       uint16
	Experience uint32
	BonusPP    byte
	Frienship  byte
	_          uint16
}

// AttacksData type
type AttacksData struct {
	Move1 uint16
	Move2 uint16
	Move3 uint16
	Move4 uint16
	PP1   byte
	PP2   byte
	PP3   byte
	PP4   byte
}

// EvData type
type EvData [12]byte

// MiscData type
type MiscData [12]byte

//
const (
	Growth  byte = iota
	Attacks byte = iota
	Ev      byte = iota
	Misc    byte = iota
)

var dataOrder = [24][4]byte{
	{Growth, Attacks, Ev, Misc},
	{Growth, Attacks, Misc, Ev},
	{Growth, Ev, Attacks, Misc},
	{Growth, Ev, Misc, Attacks},
	{Growth, Misc, Attacks, Ev},
	{Growth, Misc, Ev, Attacks},

	{Attacks, Growth, Ev, Misc},
	{Attacks, Growth, Misc, Ev},
	{Attacks, Ev, Growth, Misc},
	{Attacks, Ev, Misc, Growth},
	{Attacks, Misc, Growth, Ev},
	{Attacks, Misc, Ev, Growth},

	{Ev, Growth, Attacks, Misc},
	{Ev, Growth, Misc, Attacks},
	{Ev, Attacks, Growth, Misc},
	{Ev, Attacks, Misc, Growth},
	{Ev, Misc, Growth, Attacks},
	{Ev, Misc, Attacks, Growth},

	{Misc, Growth, Attacks, Ev},
	{Misc, Growth, Ev, Attacks},
	{Misc, Attacks, Growth, Ev},
	{Misc, Attacks, Ev, Growth},
	{Misc, Ev, Growth, Attacks},
	{Misc, Ev, Attacks, Growth},
}

// Pkmn struct
type Pkmn struct {
	Personality     uint32
	TrainerID       uint32
	Nickname        [10]byte
	Language        Language
	TrainerName     [7]byte
	Marking         Marking
	Checksum        uint16
	_               uint16
	Data            [48]byte
	StatusCondition StatusCondition
	Level           byte
	Pokerus         byte
	CurrentHealth   uint16
	TotalHealth     uint16
	AttackStat      uint16
	DefenseStat     uint16
	SpeedStat       uint16
	SpAttackStat    uint16
	SpDefenseStat   uint16
}

func decodeToBuffer(key uint32, data []byte) *bytes.Buffer {
	inputBuffer := bytes.NewBuffer(data)
	outputBuffer := new(bytes.Buffer)

	var e uint32
	var err error
	for i := 0; i <= len(data)-1; i += 4 {
		err = binary.Read(inputBuffer, binary.LittleEndian, &e)
		if err != nil {
			panic(err)
		}

		ne := e ^ key
		err = binary.Write(outputBuffer, binary.LittleEndian, ne)
		if err != nil {
			panic(err)
		}
	}

	return outputBuffer
}

func (p *Pkmn) substructureData(typ byte) *bytes.Buffer {
	key := p.TrainerID ^ p.Personality
	dataOrderIndex := p.Personality % 24
	i := bytes.IndexByte(dataOrder[dataOrderIndex][:], typ)
	if i == -1 {
		panic(errors.New("unknown data required"))
	}
	extract := p.Data[i*12 : (i+1)*12]

	return decodeToBuffer(key, extract)
}

// Growth fn
func (p *Pkmn) Growth() *GrowthData {
	buff := p.substructureData(Growth)

	g := GrowthData{}
	err := binary.Read(buff, binary.LittleEndian, &g)
	if err != nil {
		panic(err)
	}

	return &g
}

// DecodedNickname fn
func (p *Pkmn) DecodedNickname() string {
	return DecodeText(p.Nickname[:])
}

// DecodedTrainerName fn
func (p *Pkmn) DecodedTrainerName() string {
	return DecodeText(p.TrainerName[:])
}
