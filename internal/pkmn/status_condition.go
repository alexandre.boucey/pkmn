package pkmn

import (
	"bytes"
	"encoding/binary"
)

// Masks for status condition
const (
	MaskStatusConditionSleepTurn = 0b00000111
	MaskStatusConditionPoison    = 0b00001000
	MaskStatusConditionBurn      = 0b00010000
	MaskStatusConditionFreeze    = 0b00100000
	MaskStatusConditionParalysis = 0b01000000
	MaskStatusConditionBadPoison = 0b10000000
)

// StatusCondition type
type StatusCondition uint32

func (s StatusCondition) Bytes() []byte {
	outputBuffer := new(bytes.Buffer)
	_ = binary.Write(outputBuffer, binary.LittleEndian, s)

	return outputBuffer.Bytes()
}

// SleepTurn fn
func (s StatusCondition) SleepTurn() byte {
	return byte(s & MaskStatusConditionSleepTurn)
}

// IsClear fn
func (s StatusCondition) IsClear() bool {
	return s == 0
}

// HasPoison fn
func (s StatusCondition) HasPoison() bool {
	return s&MaskStatusConditionPoison == MaskStatusConditionPoison
}

// HasBurn fn
func (s StatusCondition) HasBurn() bool {
	return s&MaskStatusConditionBurn == MaskStatusConditionBurn
}

// HasFreeze fn
func (s StatusCondition) HasFreeze() bool {
	return s&MaskStatusConditionFreeze == MaskStatusConditionFreeze
}

// HasParalysis fn
func (s StatusCondition) HasParalysis() bool {
	return s&MaskStatusConditionParalysis == MaskStatusConditionParalysis
}

// HasBadPoison fn
func (s StatusCondition) HasBadPoison() bool {
	return s&MaskStatusConditionBadPoison == MaskStatusConditionBadPoison
}

func NewSleepStatusCondition(turns int) StatusCondition {
	return StatusCondition(turns & MaskStatusConditionSleepTurn)
}
