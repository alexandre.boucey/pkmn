package pkmn

type OffsetTeamPkmnField uint32
type OffsetBattlePkmnField uint32

type RomOffset struct {
	HumanName string

	PlayerTeam   uint32
	OpponentTeam uint32
	BattleTeam   uint32

	BattleTilemapA uint32
	BattleTilemapB uint32
	BattleTilemapC uint32
	BattleTilemapD uint32
}

func OffsetTeamPkmn(baseOffset uint32, n uint32, field OffsetTeamPkmnField) uint32 {
	return uint32(baseOffset + (n * 100) + uint32(field))
}

func (o RomOffset) OffsetBattlePkmn(n uint32, field OffsetBattlePkmnField) uint32 {
	return o.BattleTeam + (n * 88) + uint32(field)
}

var RomOffsets = map[string]RomOffset{
	"BPGF": {
		HumanName:      "Pokémon Vert Feuille",
		PlayerTeam:     0x02024284,
		OpponentTeam:   0x0202402C,
		BattleTeam:     0x02023BE4,
		BattleTilemapA: 0x02023FEC,
		BattleTilemapB: 0x02023FF0,
		BattleTilemapC: 0x02022BB8,
		BattleTilemapD: 0x02022BBC,
	},

	"BPRE": {
		HumanName:      "Pokémon FireRed",
		PlayerTeam:     0x02024284,
		OpponentTeam:   0x0202402C,
		BattleTeam:     0x02023BE4,
		BattleTilemapA: 0x02023FEC,
		BattleTilemapB: 0x02023FF0,
		BattleTilemapC: 0x02022BB8,
		BattleTilemapD: 0x02022BBC,
	},
}

// Memory offsets
const (
	OffsetTeamPkmnFieldPersonality   OffsetTeamPkmnField = 0
	OffsetTeamPkmnFieldCondition     OffsetTeamPkmnField = 80
	OffsetTeamPkmnFieldCurrentHealth OffsetTeamPkmnField = 86
	OffsetTeamPkmnFieldTotalHealth   OffsetTeamPkmnField = 88

	OffsetBattlePkmnFieldSpecie    OffsetBattlePkmnField = 0
	OffsetBattlePkmnFieldNickname  OffsetBattlePkmnField = 48
	OffsetBattlePkmnFieldCondition OffsetBattlePkmnField = 76
)
