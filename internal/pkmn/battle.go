package pkmn

// BattlePkmn struct
type BattlePkmn struct {
	Specie uint16

	AttackStat    uint16
	DefenseStat   uint16
	SpeedStat     uint16
	SpAttackStat  uint16
	SpDefenseStat uint16

	Move1 uint16
	Move2 uint16
	Move3 uint16
	Move4 uint16

	Ivs uint32

	HpStatBuffs        byte
	AttackStatBuffs    byte
	DefenseStatBuffs   byte
	SpeedStatBuffs     byte
	SpAttackStatBuffs  byte
	SpDefenseStatBuffs byte
	AccuracyStatBuffs  byte
	EvasionStatBuffs   byte

	AbilityID       byte
	Type1           byte
	Type2           byte
	Unk1            byte
	CurrentPP1      byte
	CurrentPP2      byte
	CurrentPP3      byte
	CurrentPP4      byte
	CurrentHealth   uint16
	Level           byte
	Happiness       byte
	TotalHealth     uint16
	HeldItem        uint16
	Nickname        [11]byte // last byte is always 0xFF ?
	Unk2            byte
	TrainerName     [8]byte // last byte is always 0xFF ?
	_               [4]byte
	Personality     uint32
	StatusCondition StatusCondition
	_               StatusCondition // second status condition is unparsed
	TrainerID       uint32
}

// DecodedNickname fn
func (p *BattlePkmn) DecodedNickname() string {
	return DecodeText(p.Nickname[:])
}

// DecodedTrainerName fn
func (p *BattlePkmn) DecodedTrainerName() string {
	return DecodeText(p.TrainerName[:])
}
