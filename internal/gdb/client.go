package gdb

import (
	"bufio"
	"encoding/hex"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"net"
	"regexp"
	"strings"
	"sync"
	"time"
)

func computeChecksum(text string) int16 {
	var sum int32 = 0
	for _, c := range text {
		sum += c
	}

	return int16(sum % 256)
}

func createMsg(text string) string {
	sb := strings.Builder{}
	sb.WriteRune('$')
	sb.WriteString(text)
	sb.WriteRune('#')
	c := computeChecksum(text)

	sb.WriteString(fmt.Sprintf("%02x", c))

	return sb.String()
}

var responseRegex = regexp.MustCompile("\\+?\\$(.*)#([a-z0-9]{2})?")
var errorConnectionClosed = errors.New("connection closed")

type Client struct {
	addr   string
	conn   net.Conn
	reader *bufio.Reader
	writer *bufio.Writer
	logger *zap.Logger
	q      *sync.Mutex
}

func (c *Client) send(cmd string) (err error) {
	if c.conn == nil {
		return errorConnectionClosed
	}
	o := createMsg(cmd)
	_, err = c.writer.WriteString(o)
	if err != nil {
		return
	}
	err = c.writer.Flush()
	c.logger.Debug("send raw", zap.String("data", o))

	return
}

func (c *Client) sendRaw(cmd []byte) (err error) {
	if c.conn == nil {
		return errorConnectionClosed
	}
	_, err = c.writer.Write(cmd)
	if err != nil {
		return
	}
	err = c.writer.Flush()
	c.logger.Debug("send raw", zap.Binary("data", cmd))

	return
}

func (c *Client) read() (res string, err error) {
	defer func() {
		if r := recover(); r != nil {
			err, ok := r.(error)
			if !ok {
				err = nil
			}

			c.logger.Debug("Recovered in read()", zap.Error(err))

			res = ""
			err = nil
		}
	}()

	if c.conn == nil {
		return "", errorConnectionClosed
	}
	cmd, err := c.reader.ReadString('#')
	if err != nil {
		return "", err
	}
	checksum := []byte{0, 0}
	_, err = c.reader.Read(checksum) // discard computeChecksum
	if err != nil {
		return "", err
	}
	cmd = cmd + string(checksum)

	c.logger.Debug("read", zap.String("data", cmd))

	m := responseRegex.FindStringSubmatch(cmd)

	return m[1], nil
}

func (c *Client) expectAck() (err error) {
	if c.conn == nil {
		return errorConnectionClosed
	}
	s, err := c.reader.ReadString('+')
	c.logger.Debug("read ack", zap.String("data", s))

	return
}

func (c *Client) executeCmd(cmd string) (string, error) {
	defer c.q.Unlock()
	c.q.Lock()
	if err := c.send(cmd); err != nil {
		return "", err
	}
	d, err := c.read()
	if err != nil {
		return "", err
	}

	return d, nil
}

func (c *Client) Continue() (err error) {
	defer c.q.Unlock()
	c.q.Lock()
	if err = c.send("c"); err != nil {
		return
	}
	err = c.expectAck()
	return
}

func (c *Client) Interrupt() (err error) {
	defer c.q.Unlock()
	c.q.Lock()
	if err = c.sendRaw([]byte{0x03}); err != nil {
		return
	}
	_, err = c.read()
	return
}

func (c *Client) Close() error {
	var err error
	if c.conn != nil {
		err = c.conn.Close()
		c.conn = nil
	}

	return err
}

func (c *Client) Connected() bool {
	return c.conn != nil
}

func (c *Client) Addr() string {
	return c.addr
}

func (c *Client) Connect() error {
	conn, err := net.DialTimeout("tcp", c.addr, time.Millisecond*200)
	if err != nil {
		return err
	}

	c.conn = conn
	c.reader = bufio.NewReader(conn)
	c.writer = bufio.NewWriter(conn)

	//TODO: not ready..
	//go func() {
	//	for c.conn != nil {
	//		c.q.Lock()
	//		c.conn.SetDeadline(time.Now().Add(5 * time.Second))
	//		cmd, err := c.read()
	//		c.q.Unlock()
	//		if errors.Is(err, os.ErrDeadlineExceeded) {
	//			continue
	//		}
	//
	//		if errors.Is(err, io.EOF) {
	//			c.Close()
	//			break
	//		}
	//
	//		if err != nil {
	//			fmt.Printf("Peekd failed: %s\n", err.Error())
	//			continue
	//		}
	//
	//		fmt.Printf("Peekd cmd %s\n", cmd)
	//		c.Continue()
	//	}
	//}()

	return nil
}

func (c *Client) ReadMemory(addr uint32, size uint16) (data []byte, err error) {
	cmd := fmt.Sprintf("m%08x,%x", addr, size)
	r, err := c.executeCmd(cmd)
	if err != nil {
		return nil, err
	}
	data, err = hex.DecodeString(r)
	return
}

func (c *Client) WriteMemory(addr uint32, data []byte) (err error) {
	cmd := fmt.Sprintf("M%08x,%x:%s", addr, len(data), hex.EncodeToString(data))
	_, err = c.executeCmd(cmd)
	return
}

//TODO: not ready..
//func (c *Client) AddWatchpoint(addr uint32) (err error) {
//	cmd := fmt.Sprintf("Z2%08x,0", addr)
//	_, err = c.executeCmd(cmd)
//	return
//}

func NewClient(addr string, logger *zap.Logger) *Client {
	return &Client{
		addr,
		nil,
		nil,
		nil,
		logger,
		&sync.Mutex{},
	}
}
