package server

import "github.com/mitchellh/hashstructure/v2"

func computeChange(v interface{}) uint64 {
	hash, err := hashstructure.Hash(v, hashstructure.FormatV2, nil)
	if err != nil {
		return 0
	}

	return hash
}

type dtoWatchTeam [6]dtoPkmn

type dtoPkmn struct {
	Personality uint32 `json:"personality"`
	ID          uint16 `json:"id"`
	Name        string `json:"name"`
	Level       byte   `json:"level"`
	CurrentHP   uint16 `json:"current_hp"`
	TotalHP     uint16 `json:"total_hp"`
	SleepTurn   byte   `json:"sleep_turn"`
	Poison      bool   `json:"poison"`
	Burn        bool   `json:"burn"`
	Freeze      bool   `json:"freeze"`
	Paralysis   bool   `json:"paralysis"`
	BadPoison   bool   `json:"bad_poison"`
}

type dtoWatchBattle struct {
	IsInCombat bool             `json:"is_in_combat"`
	Pkmn       [4]dtoBattlePkmn `json:"pkmn"`
}

type dtoBattlePkmn struct {
	Personality uint32 `json:"personality"`
	ID          uint16 `json:"id"`
	Name        string `json:"name"`
	CurrentHP   uint16 `json:"current_hp"`
	TotalHP     uint16 `json:"total_hp"`
}
