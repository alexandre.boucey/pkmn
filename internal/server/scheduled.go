package server

import (
	"go.uber.org/zap"
	"time"
)

func (s *Server) watchTeam() error {
	party, err := s.tw.GetPlayerTeam()
	if err != nil {
		return err
	}

	var event dtoWatchTeam
	for i, p := range party {
		event[i] = dtoPkmn{
			Personality: p.Personality,
			ID:          p.Growth().Specie,
			Name:        p.DecodedNickname(),
			Level:       p.Level,
			CurrentHP:   p.CurrentHealth,
			TotalHP:     p.TotalHealth,
			SleepTurn:   p.StatusCondition.SleepTurn(),
			Poison:      p.StatusCondition.HasPoison(),
			Burn:        p.StatusCondition.HasBurn(),
			Freeze:      p.StatusCondition.HasFreeze(),
			Paralysis:   p.StatusCondition.HasParalysis(),
			BadPoison:   p.StatusCondition.HasBadPoison(),
		}
	}

	s.dispatchWsEvent("party_update", event)

	return nil
}

func (s *Server) watchBattle() error {
	party, err := s.tw.GetBattle()
	if err != nil {
		return err
	}

	inCombat, err := s.tw.IsInCombat()
	if err != nil {
		return err
	}

	var event dtoWatchBattle
	event.IsInCombat = inCombat
	for i, p := range party {
		event.Pkmn[i] = dtoBattlePkmn{
			Personality: p.Personality,
			ID:          p.Specie,
			Name:        p.DecodedNickname(),
			CurrentHP:   p.CurrentHealth,
			TotalHP:     p.TotalHealth,
		}
	}

	s.dispatchWsEvent("battle_update", event)

	return nil
}

func (s *Server) executeOutOfCombat() error {
	inBattle, err := s.tw.IsInCombat()
	if err != nil {
		return err
	}

	if inBattle || len(s.outOfCombatQ) == 0 {
		return nil
	}

	for {
		var ok, valid bool
		var item OutOfCombatCmd
		select {
		case item, valid = <-s.outOfCombatQ:
			ok = true
		default:
			ok = false
		}

		if !(ok && valid) {
			break
		}

		if !item.expiresAt.IsZero() && time.Now().After(item.expiresAt) {
			s.logger.Info("Command expired in queue",
				zap.String("action", item.action),
				zap.String("target", item.target),
				zap.Time("expiresAt", item.expiresAt),
			)
		}

		switch {
		case item.target == TriggerTargetTeam && item.action == OutOfCombatActionHalveHP:
			err = s.tw.HalveTeamHP()
		case item.target == TriggerTargetRandom && item.action == OutOfCombatActionOneHP:
			err = s.tw.SetRandomToOneHP()
		case item.target == TriggerTargetTeam && item.action == OutOfCombatActionHealHP:
			err = s.tw.HealTeamHP()
		default:
			s.logger.Warn("Combination of action and target not implemented",
				zap.String("action", item.action),
				zap.String("target", item.target),
			)
		}

		if err != nil {
			return err
		}
	}

	return nil
}
