package server

import (
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
	"log"
	"net/http"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
} // use default options

type wsEvent struct {
	Type    string      `json:"type"`
	Content interface{} `json:"content"`
}

func (s *Server) actionWs(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}

	s.registerWebsocketClient(c)
}

func (s *Server) registerWebsocketClient(conn *websocket.Conn) {
	s.logger.Info("New connection from", zap.String("addr", conn.RemoteAddr().String()))

	s.wsConnPool[conn] = make(map[string]uint64)

	go func() {
		defer s.unregisterWebsocketClient(conn)
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					s.logger.Error("Client error", zap.Error(err))
				} else {
					s.logger.Info("Close received from", zap.String("addr", conn.RemoteAddr().String()))
				}
				break
			}
		}
	}()
}

func (s *Server) unregisterWebsocketClient(conn *websocket.Conn) {
	_ = conn.Close()
	s.wsConnPool[conn] = nil
	delete(s.wsConnPool, conn)
}

func (s *Server) dispatchWsEvent(eventType string, content interface{}) {
	currentHash := computeChange(content)

	for c, context := range s.wsConnPool {
		if context == nil {
			continue
		}

		if context[eventType] == currentHash {
			continue
		}

		context[eventType] = currentHash
		err := c.WriteJSON(wsEvent{eventType, content})
		if err != nil {
			s.logger.Error("Client error", zap.Error(err))
			s.unregisterWebsocketClient(c)
		}
	}
}
