package server

import (
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"math/rand"
	"net/http"
	"pkmn/internal/pkmn"
	"time"
)

const (
	TriggerTargetRandom = "random"
	TriggerTargetTeam   = "team"

	TriggerStatusTypeClear     = "clear"
	TriggerStatusTypeSleep     = "sleep"
	TriggerStatusTypePoison    = "poison"
	TriggerStatusTypeBurn      = "burn"
	TriggerStatusTypeFreeze    = "freeze"
	TriggerStatusTypeParalysis = "paralysis"

	OutOfCombatActionHalveHP = "halve"
	OutOfCombatActionOneHP   = "one"
	OutOfCombatActionHealHP  = "heal"
)

var statusMap = map[string]pkmn.StatusCondition{
	TriggerStatusTypeClear:     pkmn.StatusCondition(0),
	TriggerStatusTypeSleep:     pkmn.NewSleepStatusCondition(2),
	TriggerStatusTypePoison:    pkmn.StatusCondition(pkmn.MaskStatusConditionPoison),
	TriggerStatusTypeBurn:      pkmn.StatusCondition(pkmn.MaskStatusConditionBurn),
	TriggerStatusTypeFreeze:    pkmn.StatusCondition(pkmn.MaskStatusConditionFreeze),
	TriggerStatusTypeParalysis: pkmn.StatusCondition(pkmn.MaskStatusConditionParalysis),
}

func (s *Server) actionTriggerStatus(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	target := vars["target"]
	statusTypeKey := r.URL.Query().Get("type")
	if statusTypeKey == "" {
		// we pick a random status condition, excluding clear
		statusTypeKey = randomStatusCondition()
	}

	s.logger.Info("Triggering status change", zap.String("target", target), zap.String("statusTypeKey", statusTypeKey))
	c := statusMap[statusTypeKey]
	var err error
	switch target {
	case TriggerTargetRandom:
		err = s.tw.SetRandomStatusCondition(c)
	case TriggerTargetTeam:
		err = s.tw.SetTeamStatusCondition(c)
	default:
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

func randomStatusCondition() string {
	switch rand.Intn(5) {
	case 0:
		return TriggerStatusTypeSleep
	case 1:
		return TriggerStatusTypePoison
	case 2:
		return TriggerStatusTypeBurn
	case 3:
		return TriggerStatusTypeFreeze
	default:
		return TriggerStatusTypeParalysis
	}
}

func (s *Server) actionTriggerHp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	target := vars["target"]
	action := r.URL.Query().Get("action")
	if action == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	s.logger.Info("Triggering HP change",
		zap.String("action", action),
		zap.String("target", target),
	)
	s.outOfCombatQ <- OutOfCombatCmd{
		action:    action,
		target:    target,
		expiresAt: time.Time{},
	}

	w.WriteHeader(http.StatusNoContent)
}
