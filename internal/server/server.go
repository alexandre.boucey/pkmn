package server

import (
	"fmt"
	"go.uber.org/zap"
	"net/http"
	"pkmn/internal/live"
	"pkmn/web"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

// Server struct
type Server struct {
	tw           *live.Interactor
	wsConnPool   map[*websocket.Conn]map[string]uint64
	pollInterval time.Duration
	spriteUrl    string
	httpServer   *http.Server
	logger       *zap.Logger
	stop         chan bool
	outOfCombatQ chan OutOfCombatCmd
}

type OutOfCombatCmd struct {
	action    string
	target    string
	expiresAt time.Time
}

// NewServer fn
func NewServer(addr string, gameInteractor *live.Interactor, pollInterval time.Duration, spriteUrl string, logger *zap.Logger) *Server {

	s := &Server{
		tw:           gameInteractor,
		wsConnPool:   make(map[*websocket.Conn]map[string]uint64),
		pollInterval: pollInterval,
		spriteUrl:    spriteUrl,
		httpServer:   &http.Server{Addr: addr},
		stop:         make(chan bool),
		outOfCombatQ: make(chan OutOfCombatCmd, 1024),
		logger:       logger,
	}

	r := mux.NewRouter()
	r.HandleFunc("/ws", s.actionWs)

	r.Methods(http.MethodGet).
		Path("/sprite/{id:[0-9]+}").
		HandlerFunc(s.actionSprite)

	r.Methods(http.MethodPost).
		Path(fmt.Sprintf("/trigger/status/{target:(?:%s|%s)}", TriggerTargetRandom, TriggerTargetTeam)).
		HandlerFunc(s.actionTriggerStatus)

	r.Methods(http.MethodPost).
		Path(fmt.Sprintf("/trigger/hp/{target:(?:%s|%s)}", TriggerTargetRandom, TriggerTargetTeam)).
		HandlerFunc(s.actionTriggerHp)

	r.PathPrefix("/").Handler(web.DistSubdirFileServer(http.FS(web.WebDataFS)))

	s.httpServer.Handler = r

	return s
}

// Stop fn
func (s *Server) Stop() {
	_ = s.httpServer.Close()
}

// Listen fn
func (s *Server) Listen() {
	go func() {
		if err := s.httpServer.ListenAndServe(); err != http.ErrServerClosed {
			// unexpected error. port in use?
			s.logger.Fatal("Cannot bind server", zap.Error(err))
		}

		s.stop <- true
	}()
}

// Loop fn
func (s *Server) Loop() <-chan bool {
	extStop := make(chan bool)

	ticker := time.NewTicker(s.pollInterval)
	go func() {
		defer func() {
			s.Stop()
			extStop <- true
		}()

		var err error
		for {
			select {
			case <-s.stop:
				return
			case <-ticker.C:
				err = s.executeOutOfCombat()
				if err != nil {
					break
				}

				err = s.watchBattle()
				if err != nil {
					break
				}

				err = s.watchTeam()
				if err != nil {
					break
				}
			}

			if err != nil {
				s.logger.Error("Got error while processing tick", zap.Error(err))
				return
			}
		}
	}()

	return extStop
}

func (s *Server) actionSprite(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	pkmnId, _ := strconv.Atoi(vars["id"])
	spriteUrl := fmt.Sprintf(s.spriteUrl, pkmnId)

	w.Header().Add("Location", spriteUrl)
	w.WriteHeader(http.StatusTemporaryRedirect)
}
