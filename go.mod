module pkmn

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/mitchellh/hashstructure/v2 v2.0.1
	go.uber.org/zap v1.16.0
	gopkg.in/yaml.v2 v2.4.0
)
